const express = require('express');
const axios = require('axios');

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/entries/{source_lang}/{word_id}', (req, res) => {
    const config = {
        headers: {'Accept': 'application/json', 'app_id': 'e5212d6d', 'app_key': '65df4fbb4971eeb3bfbbbcd92d042c6f'}
    }
    axios.get('https://od-api.oxforddictionaries.com/api/v2/entries/en-us/' + req.params.word + '?fields=definitions&strictMatch=false', config).then(function(response){
        res.json(response.data)
    }).catch(function(error){
        res.status(200).send({results: '404 Not Found'})
    })
});

app.listen(3000, () => console.log('Dictionary App API listening on port 3000!'));