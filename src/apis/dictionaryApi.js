import axios from 'axios';
import AppConstants from '../constants/AppConstants';

export const getWordDefinition = (word) => {
    return axios.get(AppConstants.API_BASE_URL + '/entries/en-us/' + word + '?fields=definitions&strictMatch=false');
}