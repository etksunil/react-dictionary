import React, {Component} from 'react';

class Senses extends Component {
    
    render() {
        const index = this.props.index;
        const example = this.props.data.examples ? '‘' + this.props.data.examples[0].text + '’' : '';
        const definitions = this.props.data.senses;

        return(
            <div>
                <h5>{ index + 1 } <span>{ example }</span></h5>
                <span className={'definition-text'}>Definition</span>
                <ul>
                    { definitions && definitions.length > 0 ?
                     definitions.map((definition, i)=>
                        <li key={i}>
                        <span>{definition.definitions}{ i < definitions.length - 1 && <span key={i}></span>}</span>
                        </li>
                    )
                    :
                    ''
                    }
                </ul>
            </div>
        )
    }
    
}
export default Senses;
