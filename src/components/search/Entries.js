import React, {Component} from 'react';
import Senses from './Senses';

class Entries extends Component {

    render() {
        const data = this.props.data;
        const senses = this.props.data.entries;
        return(
            <div>
                <h5 className={'category-text'}>{data.lexicalCategory}</h5>
                {  senses && senses.length > 0 ?
                    senses.map((sense, i)=>
                        <Senses key={i} data={sense} index={i} />
                    )
                    :
                    ''
                }
                <hr />
            </div>
        )
    }
    
}

export default Entries;